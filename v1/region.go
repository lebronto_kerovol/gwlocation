package v1

type Region struct {
	Name    string  `json:"name"`
	Code    string  `json:"code"`
	Country Country `json:"country"`
}
