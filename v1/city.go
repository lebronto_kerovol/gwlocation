package v1

type City struct {
	Name   string `json:"name"`
	Region Region `json:"region"`
}
