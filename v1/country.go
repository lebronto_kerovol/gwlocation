package v1

type Country struct {
	Name                   string `json:"name"`
	Alpha_2                string `json:"alpha-2"`
	Alpha_3                string `json:"alpha-3"`
	CountryCode            string `json:"country-code"`
	ISO_3166_2             string `json:"iso31662"`
	Region                 string `json:"region"`
	SubRegion              string `json:"sub-region"`
	IntermediateRegion     string `json:"intermediate-region"`
	RegionCode             string `json:"region-code"`
	SubRegionCode          string `json:"sub-region-code"`
	IntermediateRegionCode string `json:"intermediate-region-code"`
	CustomCode             string `json:"custom-code"`
}
