package v1

import "errors"

type Location struct {
	City City `json:"city"`
}

func (location *Location) GetCountry() (*Country, error) {
	if location == nil {
		return nil, errors.New("ERROR: Location is nil.")
	}

	return &location.City.Region.Country, nil
}

func (location *Location) GetRegion() (*Region, error) {
	if location == nil {
		return nil, errors.New("ERROR: Location is nil.")
	}

	return &location.City.Region, nil
}

func (location *Location) IsLocatingInCity(city *City) (bool, error) {
	if location == nil {
		return false, errors.New("ERROR: Location is nil.")
	}

	if city == nil {
		return false, errors.New("ERROR: City is nil.")
	}

	return (location.City.Region.Country.Alpha_3 == city.Region.Country.Alpha_3) &&
		(location.City.Region.Code == city.Region.Code) &&
		(location.City.Name == city.Name), nil
}

func (location *Location) IsLocatingInRegion(region *Region) (bool, error) {
	if location == nil {
		return false, errors.New("ERROR: Location is nil.")
	}

	if region == nil {
		return false, errors.New("ERROR: Region is nil.")
	}

	return (location.City.Region.Country.Alpha_3 == region.Country.Alpha_3) &&
		(location.City.Region.Code == region.Code), nil
}

func (location *Location) IsLocatingInCountry(country *Country) (bool, error) {
	if location == nil {
		return false, errors.New("ERROR: Location is nil.")
	}

	if country == nil {
		return false, errors.New("ERROR: Country is nil.")
	}

	return (location.City.Region.Country.Alpha_3 == country.Alpha_3), nil
}
