//Make source collection object from JSON.
var language = JSON.parse(`JSON TEXT`);

//Make collection map from source collection object.
var languageCollectionMaps = [];

//Make collection map from source collection object.
language.forEach(elanguage => languageCollectionMaps.push(new Map(Object.entries(elanguage))) );

//Get unique keys.
var uniqueKeys = []

languageCollectionMaps.forEach(elanguage => {
    let keys = Array.from(elanguage.keys());
    keys.forEach(k => {
        if (uniqueKeys.includes(k) === false) {
          uniqueKeys.push(k);      
        }
    });
})


//Extended keys.
uniqueKeys.push("tandk-language-id");


//Make result map.
var fullLanguageCollectionMaps = [];
var iterator = 0;

languageCollectionMaps.forEach(eLanguage => {
    uniqueKeys.forEach(eKey => {
        let keys = Array.from(eLanguage.keys());
        if (keys.includes(eKey) === false) {
            if (eKey === "tandk-language-id") {
                eLanguage.set(eKey, iterator);
                iterator++;
            } else {
                eLanguage.set(eKey, null);
            }
        }
    });

    fullLanguageCollectionMaps.push(eLanguage);
})


//Make result collection objects.
var fullLanguageCollectionObjects = [];
var iterator = 0;

languageCollectionMaps.forEach(eLanguage => {
    var obj = {};
    for (var [key, value] of eLanguage) obj[key] = value;
    fullLanguageCollectionObjects.push(obj);
});


//Output Extended Result.
console.log(JSON.stringify(fullCountryCollectionObjects, null, "\t"))