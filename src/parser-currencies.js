//Make source object of currencies from JSON.
var objectOfCurrencies = JSON.parse(`JSON TEXT`);

//Get unique keys.
var uniqueKeys = [];

uniqueKeys = Object.keys(objectOfCurrencies);

var collectionCurrencies = [];

uniqueKeys.forEach(key => {
    currency = objectOfCurrencies[key]
    var mapCurrency = new Map();
    mapCurrency.set("iso4217", key)
    Object.keys(currency).forEach(currencyKey => {
        mapCurrency.set(currencyKey, currency[currencyKey]);
    })
    collectionCurrencies.push(Object.fromEntries(mapCurrency))
})

//console.log(JSON.stringify(collectionCurrencies))

var Wrapper = function(obj){
    if (obj == null)
        return "NULL";

    if (typeof obj === "string"){
        if (obj.length == 0)
            return "NULL";
        
        return "'"+obj+"'";
    }

    if (typeof obj === "number"){            
        return obj.toString();
    }

    return "DEFAULT";
};

var result = `INSERT INTO "currencies"("id",
"iso4217",
"symbol",
"name",
"symbol_native",
"decimal_digits",
"rounding",
"code",
"name_plural") VALUES`+"\n";

var id = 1;

collectionCurrencies.forEach(e => {
    result += "("+Wrapper(id)+","+
    Wrapper(e["iso4217"])+","+
    Wrapper(e["symbol"])+","+
    Wrapper(e["name"])+","+
    Wrapper(e["symbol-native"])+","+
    Wrapper(e["decimal-digits"])+","+
    Wrapper(e["rounding"])+","+
    Wrapper(e["code"])+","+
    Wrapper(e["name-plural"])+"),\n";
    id++;
});

console.log(result.substring(0, result.length-2)+";")

//console.log(JSON.stringify(Object.fromEntries(collectionCurrencies)))

//Make source collection object from JSON.
var language = JSON.parse(`JSON TEXT`);

//Make collection map from source collection object.
var languageCollectionMaps = [];

//Make collection map from source collection object.
language.forEach(elanguage => languageCollectionMaps.push(new Map(Object.entries(elanguage))) );

//Get unique keys.
var uniqueKeys = [];

languageCollectionMaps.forEach(elanguage => {
    let keys = Array.from(elanguage.keys());
    keys.forEach(k => {
        if (uniqueKeys.includes(k) === false) {
          uniqueKeys.push(k);      
        }
    });
})


//Extended keys.
uniqueKeys.push("tandk-language-id");


//Make result map.
var fullLanguageCollectionMaps = [];
var iterator = 0;

languageCollectionMaps.forEach(eLanguage => {
    uniqueKeys.forEach(eKey => {
        let keys = Array.from(eLanguage.keys());
        if (keys.includes(eKey) === false) {
            if (eKey === "tandk-language-id") {
                eLanguage.set(eKey, iterator);
                iterator++;
            } else {
                eLanguage.set(eKey, null);
            }
        }
    });

    fullLanguageCollectionMaps.push(eLanguage);
})


//Make result collection objects.
var fullLanguageCollectionObjects = [];
var iterator = 0;

languageCollectionMaps.forEach(eLanguage => {
    var obj = {};
    for (var [key, value] of eLanguage) obj[key] = value;
    fullLanguageCollectionObjects.push(obj);
});


//Output Extended Result.
console.log(JSON.stringify(fullCountryCollectionObjects, null, "\t"))