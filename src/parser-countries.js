var language = JSON.parse(`JSON TEXT`); //array object

var Wrapper = function(obj){
    if (obj == null)
        return "NULL";

    if (typeof obj === "string"){
        if (obj.length == 0)
            return "NULL";
        
        return "'"+obj+"'";
    }

    if (typeof obj === "number"){            
        return obj.toString();
    }

    return "DEFAULT";
};

var countries = JSON.parse(`JSON TEXT`);

var result = `INSERT INTO "countries"("id",
"continent_idx",
"phone_code",
"country_name",
"alpha_2",
"alpha_3",
"iso31662",
"region",
"sub_region",
"intermediate_region",
"region_code",
"sub_region_code",
"intermediate_region_code",
"tandk_country_id") VALUES`+"\n";

var id = 1;

countries.forEach(e => {
    result += "("+Wrapper(e["id"])+","+
    Wrapper(e["continent-idx"])+","+
    Wrapper(e["phone-code"])+","+
    Wrapper(e["country-name"])+","+
    Wrapper(e["alpha-2"])+","+
    Wrapper(e["alpha-3"])+","+
    Wrapper(e["iso31662"])+","+
    Wrapper(e["region"])+","+
    Wrapper(e["sub-region"])+","+
    Wrapper(e["intermediate-region"])+","+
    Wrapper(e["region-code"])+","+
    Wrapper(e["sub-region-code"])+","+
    Wrapper(e["intermediate-region-code"])+
    Wrapper(e["tandk-country-id"])+"),\n";
    id++;
});

console.log(result.substring(0, result.length-2)+";")